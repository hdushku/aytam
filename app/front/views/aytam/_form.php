<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model front\models\Aytam */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aytam-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'emridokumentit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nrjetimit')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'iddonator')->textInput() ?>

    <?= $form->field($model, 'nrdonatorit')->textInput() ?>

    <?= $form->field($model, 'linkufotografise')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'linkuraportitvideo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'linkuraportittekst')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
