<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model front\models\Aytam */

$this->title = Yii::t('app', 'Create Aytam');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Aytams'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aytam-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
