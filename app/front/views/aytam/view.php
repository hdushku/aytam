<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model front\models\Aytam */

$this->title = $model->nrjetimit;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Aytams'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aytam-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'emridokumentit',
            'nrjetimit',
          
            //'iddonator',
            'nrdonatorit',
          
            //'linkufotografise:url',
            
            [
                'attribute' => 'linkufotografise',
                'label' => 'Image Link',
                'format' => 'raw',
                'value' => function ($model) {
                     //return \Yii::$app->formatter->asUrl($model->linkufotografise, ['target' => '_blank']);
                     //$doc = "<a href='$model->linkuraportitvideo'>linku raportit video</a>";
                     return Html::a('Click to open', $model->linkufotografise, ['target'=>'_blank']);
                     //return $doc;
                 },
            ],
			
			[
                'attribute' => 'linkufotografise',
                'label' => 'Video Report Link',
                'format' => 'raw',
                'value' => function ($model) {
                     //return \Yii::$app->formatter->asUrl($model->linkufotografise, ['target' => '_blank']);
                     //$doc = "<a href='$model->linkuraportitvideo'>linku raportit video</a>";
                     return Html::a('Click to open', $model->linkuraportitvideo, ['target'=>'_blank']);
                     //return $doc;
                 },
            ],
			[
                'attribute' => 'linkufotografise',
                'label' => 'Text Report Link',
                'format' => 'raw',
                'value' => function ($model) {
                     //return \Yii::$app->formatter->asUrl($model->linkufotografise, ['target' => '_blank']);
                     //$doc = "<a href='$model->linkuraportitvideo'>linku raportit video</a>";
                     return Html::a('Click to open', $model->linkuraportittekst, ['target'=>'_blank']);
                     //return $doc;
                 },
            ],

            //'linkuraportitvideo:url',
            //'linkuraportittekst:url',
            //'donator.nrtelefonit',
        ],
    ]) ?>

</div>
