<?php
use yii\helpers\Html;
use yii\helpers\Url;

$photoInfo = $model->nrjetimit;
$photo = Html::img($photoInfo['url'],['alt'=>$photoInfo['alt']]);
$detailLink = Url::toRoute(['/aytam/view','id'=>$model->id]);
?>

<figure>
    <?=Html::a($photo,$detailLink,['class'=>'profile-thumb'])?>
</figure>

<ul class="details">
    <li class="btn btn-info"><span>رقم اليتيم:</span> <?=$model->nrjetimit?></li>
    <li><span>الرابط الى الصورة الشخصية:</span> <?php echo Html::a('اضفط على الرابط', $model->linkufotografise, ['target'=>'_blank']);?> </li>
    <li><span>الرابط الى التقرير فيديو:</span> <?php echo Html::a('اضفط على الرابط', $model->linkuraportitvideo, ['target'=>'_blank']);?> </li>
    <li><span>الرابط الى التقرير كتابة:</span> <?php echo Html::a('اضفط على الرابط', $model->linkuraportittekst, ['target'=>'_blank']);?> </li>
</ul>
<br>