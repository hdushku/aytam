<?php
use yii\helpers\Html;
use yii\helpers\Url;

$photoInfo = $model->nrjetimit;
$photo = Html::img($photoInfo['url'],['alt'=>$photoInfo['alt']]);
$detailLink = Url::toRoute(['/aytam/view','id'=>$model->id]);
?>

<figure>
    <?=Html::a($photo,$detailLink,['class'=>'profile-thumb'])?>
</figure>

<ul class="details">
    <li class="btn btn-info"><span>Orphan Number:</span> <?=$model->nrjetimit?></li>
    <li><span>Image Link:</span> <?php echo Html::a('Image Link', $model->linkufotografise, ['target'=>'_blank']);?> </li>
    <li><span>Video report link:</span> <?php echo Html::a('Video Report Link', $model->linkuraportitvideo, ['target'=>'_blank']);?> </li>
    <li><span>Text report link:</span> <?php echo Html::a('Text Report Link', $model->linkuraportittekst, ['target'=>'_blank']);?> </li>
</ul>
<br>