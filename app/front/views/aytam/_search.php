<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model front\models\AytamSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aytam-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>


    <?= $form->field($model, 'nrjetimit') ?>

    <?php // echo $form->field($model, 'nrdonatorit') ?>

    <?php // echo $form->field($model, 'linkufotografise') ?>

    <?php // echo $form->field($model, 'linkuraportitvideo') ?>

    <?php // echo $form->field($model, 'linkuraportittekst') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
