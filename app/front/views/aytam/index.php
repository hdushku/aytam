<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel front\models\AytamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Aytams');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aytam-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_aytam',
        'itemOptions' => ['class'=>'list-aytam'],
        //'layout' => "{sorter}\n{summary}\n{items}\n{pager}"
        //'itemOptions' => ['class' => 'item'],
        //'itemView' => function ($model, $key, $index, $widget) {
        //    return Html::a(Html::encode($model->nrjetimit), ['view', 'id' => $model->id]);
       // },
    ]) ?>
    <?php Pjax::end(); ?>
</div>
