<?php

return [
    'adminEmail' => env('APP_ADMIN_EMAIL', 'admin@example.com'),
    'languages' => [
        'ar' => 'عربي',
        'en' => 'English',
    ],
];
