<?php

namespace front\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use front\models\Aytam;
use dektrium\user\models\User;

/**
 * AytamSearch represents the model behind the search form of `front\models\Aytam`.
 */
class AytamSearch extends Aytam
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'iddonator'], 'integer'],
            [['emridokumentit', 'linkufotografise', 'linkuraportitvideo', 'linkuraportittekst'], 'safe'],
            [['nrjetimit', 'nrdonatorit'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Aytam::find()
                ->joinWith('user')
                ->where(['Aytam.user_id' => Yii::$app->user->getID()]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'pagination' => [
        	    'pagesize' => 5,
            ],
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nrjetimit' => $this->nrjetimit,
            'user_id' => $this->user_id,
            'iddonator' => $this->iddonator,
            'nrdonatorit' => $this->nrdonatorit,
        ]);

        $query->andFilterWhere(['like', 'emridokumentit', $this->emridokumentit])
            ->andFilterWhere(['like', 'linkufotografise', $this->linkufotografise])
            ->andFilterWhere(['like', 'linkuraportitvideo', $this->linkuraportitvideo])
            ->andFilterWhere(['like', 'linkuraportittekst', $this->linkuraportittekst]);

        return $dataProvider;
    }
}
