<?php

namespace front\models;

use Yii;
use dektrium\user\models\User;

/**
 * This is the model class for table "Aytam".
 *
 * @property int $id
 * @property string $emridokumentit
 * @property double $nrjetimit
 * @property int $user_id
 * @property int $iddonator
 * @property double $nrdonatorit
 * @property string $linkufotografise
 * @property string $linkuraportitvideo
 * @property string $linkuraportittekst
 *
 * @property User $user
 */
class Aytam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Aytam';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'required'],
            [['id', 'user_id', 'iddonator'], 'integer'],
            [['nrjetimit', 'nrdonatorit'], 'number'],
            [['emridokumentit', 'linkufotografise', 'linkuraportitvideo', 'linkuraportittekst'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'emridokumentit' => Yii::t('app', 'Emridokumentit'),
            'nrjetimit' => Yii::t('app', 'Nrjetimit'),
            'user_id' => Yii::t('app', 'User ID'),
            'iddonator' => Yii::t('app', 'Iddonator'),
            'nrdonatorit' => Yii::t('app', 'Nrdonatorit'),
            'linkufotografise' => Yii::t('app', 'Linkufotografise'),
            'linkuraportitvideo' => Yii::t('app', 'Linkuraportitvideo'),
            'linkuraportittekst' => Yii::t('app', 'Linkuraportittekst'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return AytamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AytamQuery(get_called_class());
    }
}
