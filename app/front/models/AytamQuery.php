<?php

namespace front\models;

/**
 * This is the ActiveQuery class for [[Aytam]].
 *
 * @see Aytam
 */
class AytamQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Aytam[]|array
     */
    public function sipasnumrit($id)
    {
        return $this->where([ 'user_id' => $id ]);
    }

    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Aytam|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
