<?php

return [
    'adminEmail' => 'admin@example.com',
    'brandLabel' => 'شعبة البلقان - دولة الكويت',
    // List of available languages for pages, news, events etc.
    // Keys must correspond to keys in UrlManager::$languages app component.
    'languages' => [
        'ar' => 'عربي',
        'en' => 'English',
    ],
];
